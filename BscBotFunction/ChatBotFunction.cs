using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using BscBotFunction.Services;
using Google.Cloud.Dialogflow.V2;
using Google.Protobuf;

namespace BscBotFunction
{
    public static class ChatBotFunction
    {
        private static readonly JsonParser jsonParser =
        new JsonParser(JsonParser.Settings.Default.WithIgnoreUnknownFields(true));



        [FunctionName("ChatBotFunction")]
        public static async Task<ContentResult> Run([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]HttpRequest req, ILogger log)
        {
            WebhookRequest request;
            using (var reader = new StreamReader(req.Body))
            {
                request = jsonParser.Parse<WebhookRequest>(reader);
            }
            AnswerService AS = new AnswerService();
            StringEditService SES = new StringEditService();

            request = SES.deviceDisplaySize(request);


            WebhookResponse response = new WebhookResponse();
            response = await AS.responseAsync(request);

            ContentResult Content = new ContentResult();
            Content.Content = response.ToString();
            Content.ContentType = "application/json; charset=utf-8";
            return Content;
        }
    }
}
