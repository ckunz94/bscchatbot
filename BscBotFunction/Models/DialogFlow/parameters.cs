﻿using System;
namespace BscBotFunction.Models.DialogFlow
{
    public class parameters
    {
        public parameters()
        {
        }

        public string DeviceColor { get; set; }
        public string DeviceType { get; set; }
        public string DeviceCapacity { get; set; }
        public string DeviceName { get; set; }
        public unitCurrency UnitCurrency { get; set; }
        public string DeviceDisplaySize { get; set; }
        public string AskiPhone { get; set; }
        public string AskiPhoneComponent { get; set; }
        public string givenName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string zipCode { get; set; }
        public string geoCity { get; set; }
        public string email { get; set; }
        public string DeviceProcessorName { get; set; }
        public string DeviceProcessorGHz { get; set; }
        public string DeviceProcessorCore { get; set; }
        public string DeviceMemory { get; set; }
        public string DeviceGraphik { get; set; }
        public addonSell[] AddonSells { get; set; }
        public string UpSell { get; set; }
        public string DeviceConnectivity { get; set; }


    }
}
