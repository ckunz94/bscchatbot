﻿using System;
namespace BscBotFunction.Models
{
    public class ParameterModel
    {
        public ParameterModel()
        {
        }
        public string key { get; set; }
        public string value { get; set; }
        public string syntax { get; set; }
    }
}
