﻿using System;
using System.Collections.Generic;
using System.Text;
using BscBotFunction.Services;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;
using Google.Cloud.Dialogflow.V2;
using Newtonsoft.Json.Linq;

namespace BscBotFunction.Services
{
    public class iPadService
    {
        DialogflowService DFS;
        public iPadService()
        {
            DFS = new DialogflowService();
        }

        public WebhookResponse buyipad(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            DatabaseService DbS = new DatabaseService();

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue != "")
            {
                string deviceDisplaySize = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue;
                if (deviceDisplaySize == "12")
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue = "12.9";
                else if (deviceDisplaySize == "10")
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue = "10.5";
                else if (deviceDisplaySize == "9")
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue = "9.7";
                else if (deviceDisplaySize == "7")
                    request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue = "7.9";

            }

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceConnectivity").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue != "")
            {

                string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;
                string deviceColor = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue;
                string deviceCapacity = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue;
                string deviceConnectivity = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceConnectivity").StringValue;
                string deviceDisplaySize = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue;




                string query1 = "SELECT DISTINCT Price, imageURL FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + deviceName + "' AND Color = '" + deviceColor + "' AND Capacity = '" + deviceCapacity + "' AND iPad_Display.Size = '" + deviceDisplaySize + "' AND Connectivity = '" + deviceConnectivity + "'";
                string[] DBresponse = DbS.readDatabase(query1);
                if (DBresponse.Length == 0 || DBresponse == null)
                {
                    response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                    return response;
                }
                string devicePrice = DBresponse[0];
                string imageURL = DBresponse[1];

                response.FulfillmentText = "Möchtest du das " + deviceName + " mit " + deviceDisplaySize + " Zoll Display, in " + deviceColor + " mit " + deviceCapacity + " GB Speicherplatz und " + deviceConnectivity + " für " + devicePrice + "€ kaufen?";


                Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Ja" });
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Nein" });
                responseCard.Title = "Konfiguration Kaufen";
                responseCard.Subtitle = response.FulfillmentText;
                responseCard.ImageUri = imageURL;

                Intent.Types.Message responseMessageCard = new Intent.Types.Message();
                responseMessageCard.Card = responseCard;
                response.FulfillmentMessages.Add(responseMessageCard);




                return response;

            }

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue == "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue == "iPad")
            {
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceType").StringValue = "iPad";
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue = "";
                response.OutputContexts.Add(DFS.addToContext("DeviceType", "iPad", request));
                response.OutputContexts.Add(DFS.addToContext("DeviceName", "", request));
                request.QueryResult.FulfillmentText = "Welches iPad möchtest du gerne haben?";
            }
            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue != "iPad Pro" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue != "" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue == "")
            {
                string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;
                string query1 = "SELECT DISTINCT iPad_Display.Size FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + deviceName + "'";
                string[] DBresponse = DbS.readDatabase(query1);
                response.OutputContexts.Add(DFS.addToContext("DeviceDisplaySize", DBresponse[0] + " Zoll", request));
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue = DBresponse[0];
                request.QueryResult.FulfillmentText = "Welche Farbe soll dein Gerät haben?";
            }
            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue == "iPad Mini" &&
                request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue == "")
            {
                string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;

                string query1 = "SELECT DISTINCT Capacity FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + deviceName + "'";
                string[] DBresponse = DbS.readDatabase(query1);
                response.OutputContexts.Add(DFS.addToContext("DeviceCapacity", DBresponse[0], request));

            }

            ConditionBuildService CBS = new ConditionBuildService();
            List<ParameterModel> parameterList = new List<ParameterModel>();
            parameterList.Add(CBS.makeParameterModel("Color", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Capacity", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("iPad.Name", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("iPad_Display.Size", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Connectivity", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceConnectivity").StringValue, "="));

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency").StructValue != null)
            {
                Google.Protobuf.WellKnownTypes.Value priceCurrencyValue = new Google.Protobuf.WellKnownTypes.Value();
                priceCurrencyValue = request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency");
                string priceStruct = priceCurrencyValue.StructValue.ToString();
                JToken UnitCurrencyJson = JToken.Parse(priceStruct);
                string price = UnitCurrencyJson["amount"].ToString();
                parameterList.Add(CBS.makeParameterModel("Price", price, "<"));
            }
            string condition = CBS.buildCondition(parameterList);
            string query = "";
            if (request.QueryResult.FulfillmentText == "Welche Farbe soll dein Gerät haben?")
                query = "SELECT DISTINCT Color FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie viel Speicherplatz möchtest du haben?")
                query = "SELECT DISTINCT Capacity FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID AND " + condition + "ORDER BY Capacity ASC";
            else if (request.QueryResult.FulfillmentText == "Welches iPad möchtest du gerne haben?")
                query = "SELECT DISTINCT iPad.Name FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID AND " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie groß soll das Display sein?")
                query = "SELECT DISTINCT iPad_Display.Size FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID AND " + condition + "ORDER BY iPad_Display.Size ASC";
            else if (request.QueryResult.FulfillmentText == "Über welche Konnektivität soll dein Gerät verfügen?")
                query = "SELECT DISTINCT Connectivity FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID AND " + condition;

            string[] dbResult = DbS.readDatabase(query);
            if (dbResult.Length == 0 || dbResult == null)
            {
                response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                return response;
            }





            Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();

            foreach (string option in dbResult)
            {
                if (request.QueryResult.FulfillmentText == "Wie viel Speicherplatz möchtest du haben?")
                    card.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = option + " GB" });
                else
                    card.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = option });
            }
            card.Title = request.QueryResult.FulfillmentText;
            Intent.Types.Message messageCard = new Intent.Types.Message();
            messageCard.Card = card;
            response.FulfillmentMessages.Add(messageCard);

            StringBuilder sb = new StringBuilder();
            sb.Append(request.QueryResult.FulfillmentText);
            sb.Append(" Zur Auswahl stehen: ");
            sb.Append(DFS.listToString(dbResult));
            if (request.QueryResult.FulfillmentText == "Wie viel Speicherplatz soll das iPhone haben?")
                sb.Append(" GigaByte.");
            else if (request.QueryResult.FulfillmentText == "Wie Groß soll das Display sein?")
                sb.Append(" Zoll.");
            else
                sb.Append(".");
            response.FulfillmentText = sb.ToString();

            return response;

        }
        public WebhookResponse buyipadUp(WebhookRequest request)
        {


            WebhookResponse response = new WebhookResponse();
            parameters parms = DFS.outputContextToParameters(request);
            DatabaseService DbS = new DatabaseService();
            DialogflowService dialogflow = new DialogflowService();




            if (request.QueryResult.FulfillmentText == "Möchtest du den Speicher oder die Konnektivität upgraden?")
            {
                response.FulfillmentText = "Sehr gute Wahl! Dann benötige ich jetzt deine Lieferdaten. Fangen wir mit deinem vollständigen Namen an: Wie heißt du?";


                response.OutputContexts.Add(dialogflow.addToContext("UpSell", "null", request));

                return response;
            }




            if (parms.DeviceDisplaySize != "")
            {
                parms.DeviceDisplaySize = parms.DeviceDisplaySize.Replace(" Zoll", "");
                string deviceDisplaySize = parms.DeviceDisplaySize;
                if (deviceDisplaySize == "12")
                    parms.DeviceDisplaySize = "12.9";
                else if (deviceDisplaySize == "10")
                    parms.DeviceDisplaySize = "10.5";
                else if (deviceDisplaySize == "9")
                    parms.DeviceDisplaySize = "9.7";
                else if (deviceDisplaySize == "7")
                    parms.DeviceDisplaySize = "7.9";

            }




            string query1 = "SELECT DISTINCT Capacity, Price FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + parms.DeviceName + "' AND Color = '" + parms.DeviceColor + "' AND iPad_Display.Size = '" + parms.DeviceDisplaySize + "' AND Connectivity = '" + parms.DeviceConnectivity + "'";
            string[] DBresponse = DbS.readDatabase(query1);
            string[] DBresponseCapacity = new string[DBresponse.Length / 2];
            string[] DBresponsePrice = new string[DBresponse.Length / 2];
            int j = 0;
            for (int i = 0; i < DBresponse.Length; i = i + 2)
            {
                DBresponseCapacity[j] = DBresponse[i];
                DBresponsePrice[j] = DBresponse[i + 1];
                j++;
            }
            string capacitySuccessor = "";
            int parmsCapacity = Int32.Parse(parms.DeviceCapacity);
            foreach (string capacity in DBresponseCapacity)
            {
                int DBcapacityint = Int32.Parse(capacity);
                if (parmsCapacity < DBcapacityint)
                {
                    capacitySuccessor = capacity;
                    break;
                }

            }

            if (parms.UpSell == "Capacity")
            {
                response.OutputContexts.Add(DFS.addToContext("DeviceCapacity", capacitySuccessor + " GB", request));
                response.FulfillmentText = "Gute Wahl. Dann benötige ich jetzt nur noch deine Lieferdaten. Fangen wir mit deinem Namen an: Wie heißt du?";
                return response;

            }
            else if (parms.UpSell == "Connectivity")
            {
                response.OutputContexts.Add(DFS.addToContext("DeviceConnectivity", "cellular+wifi", request));
                response.FulfillmentText = "Gute Wahl. Dann benötige ich jetzt nur noch deine Lieferdaten. Fangen wir mit deinem Namen an: Wie heißt du?";
                return response;

            }
            else if (parms.UpSell == "no")
            {

                response.FulfillmentText = "Alles klar. Dann benötige ich jetzt nur noch deine Lieferdaten. Fangen wir mit deinem Namen an: Wie heißt du?";
                return response;

            }


            if (capacitySuccessor != "")
            {
                int posold = 0;
                int posnew = 0;
                for (int i = 0; i < DBresponseCapacity.Length; i++)
                {
                    if (DBresponseCapacity[i] == capacitySuccessor)
                    {
                        posnew = i;
                        posold = i - 1;
                        break;
                    }
                }

                int priceOld = Int32.Parse(DBresponsePrice[posold]);
                int pricenew = Int32.Parse(DBresponsePrice[posnew]);
                int pricediff = pricenew - priceOld;

                response.FulfillmentText = "Möchtest du für dein iPad mehr Speicherplatz? Du kannst den Speicherplatz auf " + capacitySuccessor + " GB upgraden, für nur " + pricediff + " € mehr.";

                Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Speicherplatz Upgraden" });
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Nein danke" });
                responseCard.Title = "Speicherplatz upgraden?";
                responseCard.Subtitle = response.FulfillmentText;


                Intent.Types.Message responseMessageCard = new Intent.Types.Message();
                responseMessageCard.Card = responseCard;
                response.FulfillmentMessages.Add(responseMessageCard);

                return response;
            }
            else if (parms.DeviceConnectivity != "cellular+wifi")
            {

                string query2 = "SELECT DISTINCT Price FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + parms.DeviceName + "' AND Color = '" + parms.DeviceColor + "' AND iPad_Display.Size = '" + parms.DeviceDisplaySize + "' AND Capacity = '" + parms.DeviceCapacity + "'";
                string[] DBresponse1 = DbS.readDatabase(query2);

                int delta = Math.Abs(Int32.Parse(DBresponse1[0]) - Int32.Parse(DBresponse1[1]));

                response.FulfillmentText = "Möchtest du mit deinem iPad auch unterwegs ins Internet? Hole dir cellular+wifi für nur " + delta + " € mehr.";

                Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Konnektivität Upgraden" });
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Nein danke" });
                responseCard.Title = "Konnektivität upgraden?";
                responseCard.Subtitle = response.FulfillmentText;


                Intent.Types.Message responseMessageCard = new Intent.Types.Message();
                responseMessageCard.Card = responseCard;
                response.FulfillmentMessages.Add(responseMessageCard);

                return response;
            }
            response.FulfillmentText = "Sehr gute Wahl! Dann benötige ich jetzt deine Lieferdaten. Fangen wir mit deinem vollständigen Namen an: Wie heißt du?";
            response.OutputContexts.Add(dialogflow.addToContext("UpSell", "null", request));
            return response;
        }
        public async System.Threading.Tasks.Task<WebhookResponse> buyiPad_finishAsync(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            parameters parms = DFS.outputContextToParameters(request);
            DatabaseService DbS = new DatabaseService();

            parms.DeviceCapacity = parms.DeviceCapacity.Replace(" GB", "");
            parms.DeviceDisplaySize = parms.DeviceDisplaySize.Replace(" Zoll", "");

            string query1 = "SELECT DISTINCT Price FROM iPad, iPad_Display WHERE iPad.Display = iPad_Display.ID and iPad.Name = '" + parms.DeviceName + "' AND Color = '" + parms.DeviceColor + "' AND iPad_Display.Size = '" + parms.DeviceDisplaySize + "' AND Connectivity = '" + parms.DeviceConnectivity + "' AND Capacity = '" + parms.DeviceCapacity + "'";


            string devicePrice = DbS.readDatabase(query1)[0];
            unitCurrency unitCurrency = new unitCurrency();
            unitCurrency.amount = Int32.Parse(devicePrice);
            parms.UnitCurrency = unitCurrency;

            PayPalService PPS = new PayPalService();
            string paypalURL = await PPS.PayAsync(parms, request.OriginalDetectIntentRequest.Source);

            response.FulfillmentText = "Vielen Dank! Bitte folge dem folgendem link um die Zahlung durchzuführen und die Bestellung abzuschließen: "
                + paypalURL + " Nach erfolgreichem Abschluss der Bezahlung erhälst du eine E-Mail als Bestätigung.";



            Intent.Types.Message.Types.Card.Types.Button buttonBuy = new Intent.Types.Message.Types.Card.Types.Button();
            buttonBuy.Text = "jetzt kaufen";
            buttonBuy.Postback = paypalURL;
            Intent.Types.Message.Types.Card.Types.Button buttonCancle = new Intent.Types.Message.Types.Card.Types.Button();
            buttonCancle.Text = "abbrechen";


            Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();
            card.Buttons.Add(buttonBuy);
            card.Buttons.Add(buttonCancle);
            card.Title = "Mit Paypal Zahlen";
            card.Subtitle = "Vielen Dank! Nach erfolgreicher Bezahlung erhälst du eine E-Mail als Bestätigung. Klicke dazu auf 'Jetzt Kaufen'";



            Intent.Types.Message messageCard = new Intent.Types.Message();
            messageCard.Card = card;
            response.FulfillmentMessages.Add(messageCard);



            return response;
        }

    }
}
