using Google.Cloud.Dialogflow.V2;

namespace BscBotFunction.Services
{
    public class StringEditService
    {
        public StringEditService()
        {
        }
        public WebhookRequest deviceDisplaySize(WebhookRequest request)
        {
            try
            {
                Google.Protobuf.WellKnownTypes.Value textDisplaySizeOutput = new Google.Protobuf.WellKnownTypes.Value();
                bool textDisplaySizeExist = request.QueryResult.Parameters.Fields.TryGetValue("DeviceDisplaySize", out textDisplaySizeOutput);
                string textDisplaySize = textDisplaySizeOutput.StringValue;
                if (textDisplaySizeExist)
                {
                    textDisplaySize = textDisplaySize.Replace(" Zoll", "");
                    textDisplaySize = textDisplaySize.Replace("Zoll", "");
                    textDisplaySize = textDisplaySize.Replace(" ", "");
                    request.QueryResult.Parameters.Fields.Remove("DeviceDisplaySize");
                    Google.Protobuf.WellKnownTypes.Value value = new Google.Protobuf.WellKnownTypes.Value();
                    value.StringValue = textDisplaySize;
                    System.Collections.Generic.Dictionary<string, Google.Protobuf.WellKnownTypes.Value> keyValuePair = new System.Collections.Generic.Dictionary<string, Google.Protobuf.WellKnownTypes.Value>();
                    keyValuePair.Add("DeviceDisplaySize", value);
                    request.QueryResult.Parameters.Fields.Add(keyValuePair);

                }

                return request;
            }
            catch
            {
                return request;
            }

        }
        public string replaceSpaceDotAt(string input)
        {
            input = input.Replace(" ", "%20");
            input = input.Replace("@", "%40");
            return input;
        }
        public string reloadSpaceDotAt(string input)
        {
            input = input.Replace("%20", " ");
            input = input.Replace("%40", "@");
            return input;
        }
    }
}
