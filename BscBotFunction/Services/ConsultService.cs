﻿using System.Collections.Generic;
using Google.Cloud.Dialogflow.V2;
namespace BscBotFunction.Services
{
    public class ConsulService
    {
        DialogflowService DFS;
        public ConsulService()
        {
            DFS = new DialogflowService();
        }

        public WebhookResponse consult(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            string mobility = request.QueryResult.Parameters.Fields.GetValueOrDefault("ConsultMobilitaet").StringValue;
            string screen = request.QueryResult.Parameters.Fields.GetValueOrDefault("ConsultBildschirm").StringValue;
            string unterwegs = request.QueryResult.Parameters.Fields.GetValueOrDefault("ConsultUnterwegs").StringValue;
            string nutzung = request.QueryResult.Parameters.Fields.GetValueOrDefault("ConsultNutzung").StringValue;

            if (mobility == "home" && screen == "")
            {
                response.FulfillmentText = request.QueryResult.FulfillmentText;
                response.OutputContexts.Add(DFS.addToContext("ConsultBildschirm", "null", request));

            }
            else if (mobility == "mobile" && screen == "")
            {
                response.FulfillmentText = request.QueryResult.FulfillmentText;
                response.OutputContexts.Add(DFS.addToContext("ConsultBildschirm", "null", request));

            }
            if (mobility != "" && screen != "" && unterwegs != "" && nutzung != "")
            {
                if (mobility == "home")
                {
                    if (screen == "ja")
                    {
                        if (nutzung == "Spielen")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen iMac zu kaufen. Für anspruchsvolle Spiele solltest du eventuell auch den iMac Pro in Erwägung ziehen. Achte aber darauf, dass du eine gute Grafikkarte hast.";
                        }
                        else if (nutzung == "Kreativ")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen iMac Pro zu kaufen. Dieser bietet dir einen großen Bildschirm, sodass du alles gut sehen kannst. Vorallem solltest du eine gute CPU und viel Arbeitsspeicher wählen, abhängig davon ob du eher Filme (sehr viel) oder Fotos (nicht ganz so viel) erstellst";
                        }
                        else if (nutzung == "Office")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen iMac zu kaufen. Office Programme benötigen in der Regel nicht sehr viel Leistung.";
                        }
                        else if (nutzung == "swdev")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen iMac oder iMac Pro zu wählen. Dies ist abhägig davon, ob du grafiklastige Entwicklung wie zum Beispiel spiele entwickelst (iMac Pro) oder nicht (iMac). ";
                        }
                    }
                    else if (screen == "nein" || screen == "null")
                    {
                        if (nutzung == "Spielen")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen Mac Pro zu kaufen. Solltest du nicht allzu anspruchsvolle Spiele spielen, kann auch ein Mac Mini ausreichend sein.";
                        }
                        else if (nutzung == "Kreativ")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen Mac Pro zu kaufen. Falls du nur einfache Bildbearbeitung machst, kann aber auch ein gut ausgestatteter Mac Mini ausreichend sein.";
                        }
                        else if (nutzung == "Office")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen Mac Mini zu kaufen. Office Programme benötigen in der Regel nicht sehr viel Leistung.";
                        }
                        else if (nutzung == "swdev")
                        {
                            response.FulfillmentText = "Ich empfehle dir einen Mac Mini oder einen Mac Pro zu kaufen, abhängig davon, ob du einfache Spiele programmierst oder Spiele mit einer anspruchsvollen Grafik.";
                        }
                    }
                }
                else if (mobility == "mobile")
                {
                    if (unterwegs == "viel")
                    {
                        if (nutzung == "Spielen")
                        {
                            response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 13 zu kaufen. Bei anspruchsvollen Spielen ist jedoch ein MacBook Pro 15 empfehlenswerter. ";
                        }
                        else if (nutzung == "Kreativ")
                        {
                            response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 13 zu kaufen, falls du nur einfache Bildbearbeitung machst. Solltest du jedoch vorhaben anspruchsvolle Bild und Videobearbeitung durchzuführen empfehle ich dir ein MacBook pro 15.";
                        }
                        else if (nutzung == "Office")
                        {
                            response.FulfillmentText = "Ich empfehle dir entweder das MacBook oder ein MacBook Air zu kaufen. Office Programme benötigen in der Regel nicht sehr viel Leistung.";
                        }
                        else if (nutzung == "swdev")
                        {
                            response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 13 zu kaufen. Bei anspruchsvoller Computerspiele-entwicklung ist jedoch ein MacBook Pro 15 empfehlenswerter";
                        }
                    }
                    else if (unterwegs == "wenig")
                    {
                        if (nutzung == "Spielen")
                        {
                            response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 15 zu kaufen, damit bleibst du die nächsten Jahre auf dem neusten Stand und hast bei anspruchsvollen Spielen eine gute Grafikleistung.";
                        }
                        else if (nutzung == "Kreativ")
                        {
                            response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 13 zu kaufen, falls du nur einfache Bildbearbeitung machst. Solltest du jedoch vorhaben anspruchsvolle Bild und Videobearbeitung durchzuführen empfehle ich dir ein MacBook Pro 15.";
                        }
                    }
                    else if (nutzung == "Office")
                    {
                        response.FulfillmentText = "Ich empfehle dir ein MacBook Air zu kaufen. Office Programme benötigen in der Regel nicht sehr viel Leistung";
                    }
                    else if (nutzung == "swdev")
                    {
                        response.FulfillmentText = "Ich empfehle dir ein MacBook Pro 15 zu kaufen. Damit bist du in den nächsten Jahren bestens ausgestattet um auch grafisch anspruchsvolle Spiele zu entwickeln.";
                    }
                }
            }

            return response;

        }

    }
}
