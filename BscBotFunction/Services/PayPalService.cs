﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BraintreeHttp;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;

using PayPal.Core;
using PayPal.v1.Payments;

namespace BscBotFunction.Services
{
    public class PayPalService
    {
        public PayPalService()
        {

        }

        public async Task<string> PayAsync(parameters parms, string source)
        {
            string clientID = "****";
            string clientSecret = "****";
            var environment = new SandboxEnvironment(clientID, clientSecret);
            var client = new PayPalHttpClient(environment);

            source = source.Replace("?", "");
            source = source.Replace("/", "");
            if (source == "")
                source = "Google";
            if (source == " ")
                source = "Google";

            StringEditService SED = new StringEditService();
            string ReturnUrl = "https://bscbot.azurewebsites.net/api/" + parms.givenName + "/" + parms.lastName + "/" + SED.replaceSpaceDotAt(parms.email) + "/" + SED.replaceSpaceDotAt(parms.address) + "/" + parms.zipCode + "/" + parms.geoCity + "/" + SED.replaceSpaceDotAt(parms.DeviceName) + "/" + source;
            string desc = parms.DeviceName + " in " + parms.DeviceColor + " mit " + parms.DeviceCapacity + " GB Speicherplatz.";
            string fullName = parms.givenName + " " + parms.lastName;
            string price = parms.UnitCurrency.amount.ToString();
            int fullPrice = parms.UnitCurrency.amount;

            Transaction transaction = new Transaction();
            transaction.ItemList = new ItemList();
            List<Item> items = new List<Item>();


            Item product = new Item();
            product.Name = parms.DeviceName;
            product.Currency = "EUR";
            product.Price = price;
            product.Quantity = "1";
            product.Tax = "0";
            product.Description = "";
            items.Add(product);
            try
            {
                foreach (addonSell addon in parms.AddonSells)
                {
                    Item item = new Item();
                    item.Name = addon.name;
                    item.Price = addon.price.ToString();
                    item.Currency = "EUR";
                    item.Tax = "0";
                    item.Quantity = "1";
                    item.Description = "";
                    items.Add(item);
                    fullPrice += addon.price;
                }


            }
            catch { }
            transaction.ItemList.Items = items;

            Amount amount = new Amount();
            amount.Total = fullPrice.ToString();
            amount.Currency = "EUR";

            transaction.Amount = amount;

            ShippingAddress shippingAddress = new ShippingAddress();
            shippingAddress.Line1 = parms.address;
            shippingAddress.PostalCode = parms.zipCode;
            shippingAddress.City = parms.geoCity;
            shippingAddress.RecipientName = fullName;
            shippingAddress.CountryCode = "DE";

            transaction.ItemList.ShippingAddress = shippingAddress;

            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(transaction);


            var payment = new Payment()
            {
                Intent = "sale",
                Transactions = transactions,

                RedirectUrls = new RedirectUrls()
                {
                    CancelUrl = "https://bot.dialogflow.com/9b2201c5-2f8f-4222-a0ff-a185e600ed18",
                    ReturnUrl = ReturnUrl
                },
                Payer = new Payer()
                {
                    PaymentMethod = "paypal",
                    PayerInfo = new PayerInformation()
                    {
                        FirstName = parms.givenName,
                        LastName = parms.lastName,

                        ShippingAddress = new ShippingAddress()
                        {
                            Line1 = parms.address,
                            Line2 = "",
                            PostalCode = parms.zipCode,
                            City = parms.geoCity,
                            RecipientName = fullName,
                            CountryCode = "DE",

                        }
                    }

                },



            };


            PaymentCreateRequest request = new PaymentCreateRequest();
            request.RequestBody(payment);

            try
            {
                HttpResponse response = await client.Execute(request);
                var statusCode = response.StatusCode;
                Payment result = response.Result<Payment>();
                return result.Links[1].Href;
            }
            catch (HttpException httpException)
            {
                var statusCode = httpException.StatusCode;
                var debugId = httpException.Headers.GetValues("PayPal-Debug-Id").FirstOrDefault();
                return "error";
            }
        }
    }
}
