using System;
using System.Collections.Generic;
using System.Text;
using BscBotFunction.Models;
using BscBotFunction.Models.DialogFlow;
using Google.Cloud.Dialogflow.V2;
using Newtonsoft.Json.Linq;
namespace BscBotFunction.Services
{
    public class iPhoneService
    {
        DialogflowService DFS;
        public iPhoneService()
        {
            DFS = new DialogflowService();
        }
        public WebhookResponse buyiphone(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            DatabaseService DbS = new DatabaseService();

            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue != "" && request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue != "" && request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue != "")
            {
                // buy specific iphone 
                string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;
                string deviceColor = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue;
                string deviceCapacity = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue;
                string query1 = "SELECT DISTINCT Price, imageURL FROM iPhone WHERE Name = '" + deviceName + "' AND Color = '" + deviceColor + "' AND Capacity = '" + deviceCapacity + "'";
                string[] DBresponse = DbS.readDatabase(query1);
                if (DBresponse.Length == 0 || DBresponse == null)
                {
                    response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                    return response;
                }

                string devicePrice = DBresponse[0];
                string imageURL = DBresponse[1];

                response.FulfillmentText = "Möchtest du das " + deviceName + " in " + deviceColor + " mit " + deviceCapacity + " GB Speicherplatz für " + devicePrice + "€ kaufen?";


                Intent.Types.Message.Types.Card responseCard = new Intent.Types.Message.Types.Card();
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Ja" });
                responseCard.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = "Nein" });
                responseCard.Title = "Konfiguration Kaufen";
                responseCard.Subtitle = response.FulfillmentText;
                responseCard.ImageUri = imageURL;

                Intent.Types.Message responseMessageCard = new Intent.Types.Message();
                responseMessageCard.Card = responseCard;
                response.FulfillmentMessages.Add(responseMessageCard);


                return response;

            }



            ConditionBuildService CBS = new ConditionBuildService();
            List<ParameterModel> parameterList = new List<ParameterModel>();
            parameterList.Add(CBS.makeParameterModel("Color", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceColor").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Capacity", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceCapacity").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("Name", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue, "="));
            parameterList.Add(CBS.makeParameterModel("iPhone_Display.Size", request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceDisplaySize").StringValue, "="));



            if (request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency").StructValue != null)
            {
                Google.Protobuf.WellKnownTypes.Value priceCurrencyValue = new Google.Protobuf.WellKnownTypes.Value();
                priceCurrencyValue = request.QueryResult.Parameters.Fields.GetValueOrDefault("UnitCurrency");
                string priceStruct = priceCurrencyValue.StructValue.ToString();
                JToken UnitCurrencyJson = JToken.Parse(priceStruct);
                string price = UnitCurrencyJson["amount"].ToString();
                parameterList.Add(CBS.makeParameterModel("Price", price, "<"));
            }

            string condition = CBS.buildCondition(parameterList);
            string query = "";
            if (request.QueryResult.FulfillmentText == "Welche Farbe soll das iPhone haben?")
                query = "SELECT DISTINCT Color FROM iPhone, iPhone_Display WHERE " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie viel Speicherplatz soll das iPhone haben?")
                query = "SELECT DISTINCT Capacity FROM iPhone, iPhone_Display WHERE " + condition + "ORDER BY Capacity ASC";
            else if (request.QueryResult.FulfillmentText == "Welches Modell möchtest du haben?")
                query = "SELECT DISTINCT Name FROM iPhone, iPhone_Display WHERE " + condition;
            else if (request.QueryResult.FulfillmentText == "Wie Groß soll das Display sein?")
                query = "SELECT DISTINCT iPhone_Display.Size FROM iPhone, iPhone_Display WHERE " + condition + "ORDER BY iPhone_Display.Size ASC";

            string[] dbResult = DbS.readDatabase(query);
            if (dbResult.Length == 0 || dbResult == null)
            {
                response.FulfillmentText = "Für die gewählte Konfiguration konnte ich leider kein Gerät finden.";
                return response;
            }
            if (request.OriginalDetectIntentRequest.Source == "slack_testbot" && request.QueryResult.FulfillmentText == "Welches Modell möchtest du haben?")
            {
                Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();
                Intent.Types.Message.Types.Card card2 = new Intent.Types.Message.Types.Card();

                for (int i = 0; i < 5; i++)
                {
                    card.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = dbResult[i] });
                }
                for (int i = 5; i < dbResult.Length; i++)
                {
                    card2.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = dbResult[i] });
                }
                card.Title = request.QueryResult.FulfillmentText;
                Intent.Types.Message messageCard = new Intent.Types.Message();
                messageCard.Card = card;
                response.FulfillmentMessages.Add(messageCard);
                card2.Title = " ";
                Intent.Types.Message messageCard2 = new Intent.Types.Message();
                messageCard2.Card = card2;
                response.FulfillmentMessages.Add(messageCard2);
            }
            else
            {
                Intent.Types.Message.Types.Card card = new Intent.Types.Message.Types.Card();

                foreach (string option in dbResult)
                {
                    card.Buttons.Add(new Intent.Types.Message.Types.Card.Types.Button() { Text = option });
                }
                card.Title = request.QueryResult.FulfillmentText;
                Intent.Types.Message messageCard = new Intent.Types.Message();
                messageCard.Card = card;


                response.FulfillmentMessages.Add(messageCard);

            }

            StringBuilder sb = new StringBuilder();
            sb.Append(request.QueryResult.FulfillmentText);
            sb.Append(" Zur Auswahl stehen: ");
            sb.Append(DFS.listToString(dbResult));
            if (request.QueryResult.FulfillmentText == "Wie viel Speicherplatz soll das iPhone haben?")
                sb.Append(" GigaByte.");
            else if (request.QueryResult.FulfillmentText == "Wie Groß soll das Display sein?")
                sb.Append(" Zoll.");
            else
                sb.Append(".");
            response.FulfillmentText = sb.ToString();

            return response;

        }
        public async System.Threading.Tasks.Task<WebhookResponse> buyiphone_finishAsync(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();
            parameters parms = DFS.outputContextToParameters(request);

            string deviceName = parms.DeviceName;
            string deviceColor = parms.DeviceColor;
            string deviceCapacity = parms.DeviceCapacity;
            string query1 = "SELECT DISTINCT Price FROM iPhone WHERE Name = '" + deviceName + "' AND Color = '" + deviceColor + "' AND Capacity = '" + deviceCapacity + "'";
            DatabaseService DbS = new DatabaseService();
            string devicePrice = DbS.readDatabase(query1)[0];
            unitCurrency unitCurrency = new unitCurrency();
            unitCurrency.amount = Int32.Parse(devicePrice);
            parms.UnitCurrency = unitCurrency;

            PayPalService PPS = new PayPalService();
            string paypalURL = await PPS.PayAsync(parms, request.OriginalDetectIntentRequest.Source);

            response.FulfillmentText = "Vielen Dank! Bitte klicke auf folgenden Link um die Zahlung durchzuführen und die Bestellung abzuschließen: "
                + paypalURL + " Nach erfolgreichem Abschluss der Bezahlung erhälst du eine E-Mail als Bestätigung.";


            return response;
        }
        public WebhookResponse askiphone(WebhookRequest request)
        {
            WebhookResponse response = new WebhookResponse();


            DatabaseService DbS = new DatabaseService();

            string askiPhone = request.QueryResult.Parameters.Fields.GetValueOrDefault("AskiPhone").StringValue;
            string askiPhoneComponent = request.QueryResult.Parameters.Fields.GetValueOrDefault("AskiPhoneComponent").StringValue;
            string deviceName = request.QueryResult.Parameters.Fields.GetValueOrDefault("DeviceName").StringValue;
            string query = "";


            if (askiPhoneComponent == "" && deviceName != "")
            {
                if (askiPhone == "Speicherplatz")
                {
                    query = "SELECT DISTINCT Capacity FROM iPhone WHERE Name = '" + deviceName + "ORDER BY Capacity ASC" + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " verfügt über " + DFS.listToString(dbResult) + " Gigabyte Speicherplatz.";

                }
                else if (askiPhone == "Farbe")
                {
                    query = "SELECT DISTINCT Color FROM iPhone WHERE Name = '" + deviceName + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " ist in den Farben " + DFS.listToString(dbResult) + " verfügbar.";

                }
                else if (askiPhone == "Gewicht")
                {
                    query = "SELECT DISTINCT Weight FROM iPhone WHERE Name = '" + deviceName + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " wiegt " + DFS.listToString(dbResult) + " Gramm.";

                }
                else if (askiPhone == "Biometrie")
                {
                    query = "SELECT DISTINCT Biometrie FROM iPhone WHERE Name = '" + deviceName + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " setzt als Biometrischen Sensor " + DFS.listToString(dbResult) + " ein.";

                }
                else if (askiPhone == "ApplePay")
                {
                    query = "SELECT DISTINCT ApplePay FROM iPhone WHERE Name = '" + deviceName + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    if (dbResult[0] == "ja")
                        response.FulfillmentText = "Apple Pay ist mit dem " + deviceName + " möglich.";
                    else if ((dbResult[0] == "nein"))
                        response.FulfillmentText = "Apple Pay ist mit dem " + deviceName + " leider nicht möglich.";
                    else
                        response.FulfillmentText = "Das weis ich leider nicht.";
                }
                else if (askiPhone == "Preis")
                {
                    query = "SELECT DISTINCT Price FROM iPhone WHERE Name = '" + deviceName + "';";

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " kostet abhängig von der Ausstattung " + DFS.listToString(dbResult) + " Euro.";

                }
                else if (askiPhone == "Groesse")
                {
                    query = "SELECT DISTINCT iPhone_Size.Height FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    string[] dbResult = DbS.readDatabase(query);
                    string height = dbResult[0];

                    query = "SELECT DISTINCT iPhone_Size.Depth FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    dbResult = DbS.readDatabase(query);
                    string depth = dbResult[0];

                    query = "SELECT DISTINCT iPhone_Size.Width FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    dbResult = DbS.readDatabase(query);
                    string width = dbResult[0];

                    response.FulfillmentText = "Das " + deviceName + " hat die Maße: " + height + " x " + width + " x " + depth;
                }



            }
            else if (askiPhoneComponent == "Size")
            {
                if (askiPhone == "")
                {
                    query = "SELECT DISTINCT iPhone_Size.Height FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    string[] dbResult = DbS.readDatabase(query);
                    string height = dbResult[0];

                    query = "SELECT DISTINCT iPhone_Size.Depth FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    dbResult = DbS.readDatabase(query);
                    string depth = dbResult[0];

                    query = "SELECT DISTINCT iPhone_Size.Width FROM iPhone, iPhone_Size WHERE Name = '" + deviceName + "' and iPhone.Size = iPhone_Size.ID;";
                    dbResult = DbS.readDatabase(query);
                    string width = dbResult[0];

                    response.FulfillmentText = "Das " + deviceName + " hat die Maße: " + height + " x " + width + " x " + depth;
                }
            }
            else if (askiPhoneComponent == "Display")
            {
                string from = "iPhone, iPhone_Display WHERE Name = '" + deviceName + "' and iPhone.Display = iPhone_Display.ID; ";
                if (askiPhone == "")
                {

                }
                else if (askiPhone == "Groesse")
                {
                    query = "SELECT DISTINCT iPhone_Display.Size FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " hat ein Display der Größe " + dbResult[0] + " Zoll.";
                }
                else if (askiPhone == "Technology")
                {
                    query = "SELECT DISTINCT iPhone_Display.Technology FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " verwendet im Display " + dbResult[0] + ".";

                }
                else if (askiPhone == "HDR")
                {
                    query = "SELECT DISTINCT iPhone_Display.HDR FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " verfügt über HDR im Display: " + dbResult[0] + ".";

                }
                else if (askiPhone == "Aufloesung")
                {
                    query = "SELECT DISTINCT iPhone_Display.Resolution FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " hat eine Auflösung von " + dbResult[0] + ".";

                }
                else if (askiPhone == "PPI")
                {
                    query = "SELECT DISTINCT iPhone_Display.PPI FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Der Display des " + deviceName + " hat " + dbResult[0] + " PPI.";

                }
                else if (askiPhone == "Kontrast")
                {
                    query = "SELECT DISTINCT iPhone_Display.Contrast FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " hat einen Kontrast von " + dbResult[0] + ".";

                }
                else if (askiPhone == "TrueTone")
                {
                    query = "SELECT DISTINCT iPhone_Display.TrueTone FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " besitzt die TrueTone Technologie: " + dbResult[0] + ".";

                }
                else if (askiPhone == "3DTouch")
                {
                    query = "SELECT DISTINCT iPhone_Display.ThreeDTouch FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Das " + deviceName + " besitzt die 3D Touch Technologie: " + dbResult[0] + ".";

                }
                else if (askiPhone == "maxHelligkeit")
                {
                    query = "SELECT DISTINCT iPhone_Display.Size FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);
                    response.FulfillmentText = "Die maximale Helligkeit des " + deviceName + " liegt bei " + dbResult[0] + " cd pro Quadratmeter.";

                }
            }
            else if (askiPhoneComponent == "Camera")
            {
                string from = "iPhone, iPhone_Camera WHERE Name = '" + deviceName + "' and iPhone.Camera = iPhone_Camera.ID; ";


                if (askiPhone == "Megapixel")
                {
                    query = "SELECT DISTINCT Megapixel FROM " + from;

                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Die Kamera des " + deviceName + " verfügt über " + DFS.listToString(dbResult) + " Megapixel.";

                }
                else if (askiPhone == "Blende")
                {
                    List<ParameterModel> parameters = new List<ParameterModel>();

                    query = "SELECT DISTINCT WideAngelAperture FROM " + from;
                    parameters.Add(DFS.makeParameter("Weitwinkelblende", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT TelephotoLensAperture FROM " + from;
                    parameters.Add(DFS.makeParameter("Telephotolinsenblende", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT Aperture FROM " + from;
                    parameters.Add(DFS.makeParameter("Blende", DbS.readDatabase(query)[0]));

                    response.FulfillmentText = "Die Kamera des " + deviceName + " verfügt über folgende Blende(n): " + DFS.parameterListToString(parameters) + ".";

                }

            }
            else if (askiPhoneComponent == "FrontCamera")
            {
                string from = "iPhone, iPhone_FrontCamera WHERE iPhone.Name = '" + deviceName + "' and iPhone.FrontCamera = iPhone_FrontCamera.ID; ";

                if (askiPhone == "Megapixel")
                {
                    query = "SELECT DISTINCT iPhone_FrontCamera.Megapixel FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Die Frontkamera des " + deviceName + " hat " + DFS.listToString(dbResult) + " Megapixel.";

                }
                else if (askiPhone == "Blende")
                {
                    query = "SELECT DISTINCT iPhone_FrontCamera.Aperture FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Die Blende der Frontkamera des " + deviceName + " hat einen Wert von " + DFS.listToString(dbResult) + ".";

                }
                else if (askiPhone == "Name")
                {
                    query = "SELECT DISTINCT iPhone_FrontCamera.Name FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Die Frontkamera des " + deviceName + " heist " + DFS.listToString(dbResult) + ".";

                }
            }
            else if (askiPhoneComponent == "IPProtection")
            {
                string from = "iPhone, iPhone_IPProtection WHERE iPhone.Name = '" + deviceName + "' and iPhone.IPProtection = iPhone_IPProtection.ID; ";

                if (askiPhone == "")
                {
                    List<ParameterModel> parameters = new List<ParameterModel>();


                    query = "SELECT DISTINCT Depth FROM " + from;
                    parameters.Add(DFS.makeParameter("Tauchtiefe:", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT Duration FROM " + from;
                    parameters.Add(DFS.makeParameter("Tauchlänge:", DbS.readDatabase(query)[0]));

                    query = "SELECT DISTINCT iPhone_IPProtection.ID FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " ist nach dem Standard " + DFS.listToString(dbResult) + " geschützt, was einen Schutz gewährt für " + DFS.parameterListToString(parameters) + ".";

                }
                else if (askiPhone == "Tiefe")
                {
                    query = "SELECT DISTINCT Depth FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " ist bis " + DFS.listToString(dbResult) + " Unterwasser geschützt.";

                }
                else if (askiPhone == "Dauer")
                {
                    query = "SELECT DISTINCT Duration FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Das " + deviceName + " ist für " + DFS.listToString(dbResult) + " Unterwasser geschützt.";

                }
            }
            else if (askiPhoneComponent == "Locating")
            {
                string from = "iPhone, iPhone_Locating WHERE iPhone.Name = '" + deviceName + "' and iPhone.Locating = iPhone_Locating.ID; ";

                List<ParameterModel> parameters = new List<ParameterModel>();


                query = "SELECT DISTINCT GPS FROM " + from;
                parameters.Add(DFS.makeParameter("GPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT DigitalCompass FROM " + from;
                parameters.Add(DFS.makeParameter("Digitaler Kompass:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT WLAN FROM " + from;
                parameters.Add(DFS.makeParameter("WLAN:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT Mobile FROM " + from;
                parameters.Add(DFS.makeParameter("Mobile:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT iBeacon FROM " + from;
                parameters.Add(DFS.makeParameter("iBeacon:", DbS.readDatabase(query)[0]));




                response.FulfillmentText = "Das " + deviceName + " verfügt über folgende Lokalisierungsmöglichkeiten: " + DFS.parameterListToString(parameters) + ".";

            }
            else if (askiPhoneComponent == "PowerSupply")
            {
                string from = "iPhone, iPhone_PowerSupply WHERE iPhone.Name = '" + deviceName + "' and iPhone.PowerSupply = iPhone_PowerSupply.ID; ";

                if (askiPhone == "" || askiPhone == "Dauer")
                {
                    List<ParameterModel> parameters = new List<ParameterModel>();


                    query = "SELECT DISTINCT TalkTime FROM " + from;
                    parameters.Add(DFS.makeParameter("Sprechdauer:", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT Standby FROM " + from;
                    parameters.Add(DFS.makeParameter("Standby:", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT InternetUse FROM " + from;
                    parameters.Add(DFS.makeParameter("Internet Nutzung:", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT WirelessVideoPlayback FROM " + from;
                    parameters.Add(DFS.makeParameter("Videowiedergabe (drahtlos)", DbS.readDatabase(query)[0]));
                    query = "SELECT DISTINCT WirelessAudioPlayback FROM " + from;
                    parameters.Add(DFS.makeParameter("Audiowiedergabe (drahtlos):", DbS.readDatabase(query)[0]));




                    response.FulfillmentText = "Der Akku des " + deviceName + " reicht für folgende Aktivitäten: " + DFS.parameterListToString(parameters) + ".";

                }
                else if (askiPhone == "Sprechzeit")
                {
                    query = "SELECT DISTINCT TalkTime FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Mit dem " + deviceName + " ist eine Sprechdauer von bis zu " + DFS.listToString(dbResult) + " möglich.";

                }
                else if (askiPhone == "InternetNutzung")
                {
                    query = "SELECT DISTINCT InternetUse FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Mit dem " + deviceName + " ist eine Internetnutzung von bis zu " + DFS.listToString(dbResult) + " möglich.";

                }
                else if (askiPhone == "Videowiedergabe")
                {
                    query = "SELECT DISTINCT WirelessVideoPlayback FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Mit dem " + deviceName + " ist eine Videowiedergabe (drahtlos) von bis zu " + DFS.listToString(dbResult) + " möglich.";

                }
                else if (askiPhone == "Audiowiedergabe")
                {
                    query = "SELECT DISTINCT WirelessAudioPlayback FROM " + from;
                    string[] dbResult = DbS.readDatabase(query);

                    response.FulfillmentText = "Mit dem " + deviceName + " ist eine Audiowiedergabe (drahtlos) von bis zu " + DFS.listToString(dbResult) + " möglich.";

                }
            }
            else if (askiPhoneComponent == "Processor")
            {
                string from = "iPhone, iPhone_Processor WHERE iPhone.Name = '" + deviceName + "' and iPhone.Processor = iPhone_Processor.ID; ";
                query = "SELECT DISTINCT iPhone_Processor.Name FROM " + from;
                string[] dbResult = DbS.readDatabase(query);

                response.FulfillmentText = "Das " + deviceName + " verfügt über einen " + DFS.listToString(dbResult) + ".";

            }
            else if (askiPhoneComponent == "Sensors")
            {
                string from = "iPhone, iPhone_Sensors WHERE iPhone.Name = '" + deviceName + "' and iPhone.Sensors = iPhone_Sensors.ID; ";
                List<ParameterModel> parameters = new List<ParameterModel>();


                query = "SELECT DISTINCT Barometer FROM " + from;
                parameters.Add(DFS.makeParameter("Barometer:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT ThreeAxisGyro FROM " + from;
                parameters.Add(DFS.makeParameter("3-Achsen Gyrosensor:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT Accelerometer FROM " + from;
                parameters.Add(DFS.makeParameter("Beschleunigungs­sensor:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT ProximitySensor FROM " + from;
                parameters.Add(DFS.makeParameter("Näherungssensor:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT AmbientLightSensor FROM " + from;
                parameters.Add(DFS.makeParameter("Umgebungs­lichtsensor:", DbS.readDatabase(query)[0]));




                response.FulfillmentText = "Das " + deviceName + " verfügt über folgende Sensoren: " + DFS.parameterListToString(parameters) + ".";

            }
            else if (askiPhoneComponent == "Sim")
            {
                string from = "iPhone, iPhone_Sim WHERE iPhone.Name = '" + deviceName + "' and iPhone.SimCard = iPhone_Sim.ID; ";
                List<ParameterModel> parameters = new List<ParameterModel>();


                query = "SELECT DISTINCT NanoSim FROM " + from;
                parameters.Add(DFS.makeParameter("Nano-Sim:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT ESim FROM " + from;
                parameters.Add(DFS.makeParameter("E-Sim:", DbS.readDatabase(query)[0]));

                response.FulfillmentText = "Als Simkarten können beim " + deviceName + " folgende verwendet werden: " + DFS.parameterListToString(parameters) + ".";

            }
            else if (askiPhoneComponent == "VideoRecording")
            {
                string from = "iPhone, iPhone_VideoRecording WHERE iPhone.Name = '" + deviceName + "' and iPhone.VideoRecording = iPhone_VideoRecording.ID; ";
                List<ParameterModel> parameters = new List<ParameterModel>();


                query = "SELECT DISTINCT x4K_24fps FROM " + from;
                parameters.Add(DFS.makeParameter("4K mit 24 FPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT x4K_30fps FROM " + from;
                parameters.Add(DFS.makeParameter("4K mit 30 FPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT x4K_60fps FROM " + from;
                parameters.Add(DFS.makeParameter("4K mit 60 FPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT x1080p_30fps FROM " + from;
                parameters.Add(DFS.makeParameter("1080p mit 30 FPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT x1080p_60fps FROM " + from;
                parameters.Add(DFS.makeParameter("1080p mit 60 FPS:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT x720p_30fps FROM " + from;
                parameters.Add(DFS.makeParameter("720p mit 30 FPS:", DbS.readDatabase(query)[0]));

                response.FulfillmentText = "Das " + deviceName + " kann in folgenden Formaten Videos aufnehmen: " + DFS.parameterListToString(parameters) + ".";

            }
            else if (askiPhoneComponent == "Wireless")
            {
                string from = "iPhone, iPhone_Wireless WHERE iPhone.Name = '" + deviceName + "' and iPhone.Wireless = iPhone_Wireless.ID; ";
                List<ParameterModel> parameters = new List<ParameterModel>();


                query = "SELECT DISTINCT LTE FROM " + from;
                parameters.Add(DFS.makeParameter("LTE:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT WLAN FROM " + from;
                parameters.Add(DFS.makeParameter("WLAN:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT Bluetooth FROM " + from;
                parameters.Add(DFS.makeParameter("Bluetooth:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT NFC FROM " + from;
                parameters.Add(DFS.makeParameter("NFC:", DbS.readDatabase(query)[0]));
                query = "SELECT DISTINCT ExpressCard FROM " + from;
                parameters.Add(DFS.makeParameter("Express Card:", DbS.readDatabase(query)[0]));

                response.FulfillmentText = "Das " + deviceName + " verfügt über die kabellos Technologien: " + DFS.parameterListToString(parameters) + ".";

            }

            return response;
        }
    }
}
